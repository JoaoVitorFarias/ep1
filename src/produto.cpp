#include "produto.hpp"
#include <iostream>

Produto :: Produto(){
    categoria = "Unknown";
    nomeProd = "Unknown";
    marcaProd = "Unknown";
    precoProd = 0;
    qntProd = 0;
    codProd = '0';
}
Produto :: Produto(string categoria, string nomeProd, string marcaProd, float precoProd, int qntProd, string codProd){
    setCategoria(categoria);
    setNomeProd(nomeProd);
    setMarcaProd(marcaProd);
    setPrecoProd(precoProd);
    setQntProd(qntProd);
    setCodProd(codProd);
}
Produto :: ~Produto(){
}
void Produto :: setCategoria(string categoria){
    this->categoria = categoria;
}
string Produto :: getCategoria(){
    return categoria;
}
void Produto :: setNomeProd(string nomeProd){
    this->nomeProd = nomeProd;
}
string Produto :: getNomeProd(){
    return nomeProd;
}
void Produto :: setMarcaProd(string marcaProd){
    this->marcaProd = marcaProd;
}
string Produto :: getMarcaProd(){
    return marcaProd;
}
void Produto :: setPrecoProd(float precoProd){
    if (precoProd < 0){
        precoProd = 0;
    }
    this->precoProd = precoProd;
}
float Produto :: getPrecoProd(){
    return precoProd;
}
void Produto :: setQntProd (int qntProd){
    if (qntProd < 0){
        qntProd = 0;
    }
    this->qntProd = qntProd;
}
int Produto :: getQntProd(){
    return qntProd;
}
void Produto :: setCodProd(string codProd){
    this->codProd = codProd;
}
string Produto :: getCodProd(){
    return codProd;
}