#include "cliente.hpp"
#include <iostream>

Cliente :: Cliente (){
    socio = false;
    nome = "Unknown";  
    idade = 18;
    rg = '0';
    cpf = '0';
    email = "unknown@gmail.com";
}
Cliente :: Cliente (bool socio){
    setSocio(socio);    
    nome = "Unknown";  
    idade = 18;
    rg = '0';
    cpf='0';
    email = "unknown@gmail.com";

}

Cliente :: Cliente (bool socio, string nome, int idade, string rg, string cpf, string email){
    setSocio(socio);
    setNome(nome);
    setIdade(idade);
    setRG(rg);
    setCPF(cpf);
    setEmail(email);    
}
Cliente :: ~Cliente (){
}
void Cliente :: setSocio(bool socio){
    if (socio!= true || socio!= false){
        //throw();
    }else{
        this->socio = socio;
    }
}
bool Cliente :: getSocio(){
    return socio;
}
void Cliente :: setNome(string nome){
    this->nome = nome;    
}
string Cliente :: getNome (){
    return nome;    
}
void Cliente :: setIdade (int idade){
    if (idade < 0){
    idade = 18;
    }
    this->idade= idade;

}
int Cliente :: getIdade(){
    return idade;  
}
void Cliente :: setRG(string rg){
    this->rg = rg;
}
string Cliente :: getRG(){
    return rg;  
}
void Cliente :: setCPF(string cpf){
    this->cpf = cpf;
}
string Cliente :: getCPF (){
    return cpf;    
}
void Cliente :: setEmail(string email){
    this->email = email;    
}
string Cliente :: getEmail(){
    return email;    
}