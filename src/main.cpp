#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>

#include "produto.hpp"
#include "cliente.hpp"
#include "carrinho.hpp"


using namespace std;

int login (string cpf);
int seraqueeSocio (string cpf);
int buscaProduto ( string cod);
int verificaEstoque(string cod, int qnt);
void atualizaProduto(vector <Carrinho *> &car);
void printcarrinho(vector <Carrinho *> &car);
float fechaConta(vector <Carrinho *> &car);
int atualizaQnt(int qnt, string cod);
void printProduto(string cod);
void addRecomendacao (string cpf, vector <Carrinho *> &car);
void Recomenda (string cpf);


int main (){
    printf("\t\tBEM-VINDO A LOJA DA VICTORIA\n");
    int opcao, idade_novo, opcaoCompra, quantidadeProd, emFalta = 0, eSocio, opcaoProduto; 
    float valor, valorFinal, precoProd;
    string nome_novo,email_novo, cpf, cpf_novo, rg_novo ,codigoProd, nomePod, marcaProd, categoriaProd, a;
    char cliente_cadastrado;
    bool socio;
    ofstream arqCliente;
    ofstream arqProduto;
    vector <Carrinho *> carrinho;
    Cliente socio_novo;
    Cliente cliente_novo;
    Produto produto_novo;
    do{
        cout << "[1] MODO VENDA" << endl<<"[2] MODO ESTOQUE" << endl<< "[3] MODO RECOMENDACAO" << endl << "[0] SAIR" << endl;
        cin >> opcao;
        getchar();
        system("clear");
        switch (opcao){
        case 1 :
            cout << endl << "CLIENTE JA CADASTRADO? [S/N]" << endl;
            cin >> cliente_cadastrado;
            getchar();
            system("clear");
               if (cliente_cadastrado=='s' || cliente_cadastrado == 'S'){
                printf ("\t LOGIN:\n");
                cout << "CPF: ";
                cin >> cpf;
                if (login (cpf) != 0){ //verificando se o cliente está realmente cadastrado
                    cout << "ERRO: CLIENTE NAO CADASTRADO"<< endl;
                    getchar();
                    getchar();
                    system("clear");
                    break; 
                }
                getchar();
                system("clear");
                eSocio = seraqueeSocio(cpf);
                printf("\t\t CARRINHO\n");
                do {  
                    cout << "CODIGO DO PRODUTO: ";
                    cin >>  codigoProd;
                    cout << "QUANTIDADE:";
                    cin >> quantidadeProd;
                    if (buscaProduto (codigoProd)!= 0){
                        cout << "PRODUTO INEXISTENTE."<< endl;
                    }else{
                        carrinho.push_back(new Carrinho (quantidadeProd, codigoProd));
                    }
                    cout << "[1]ADICIONAR PRODUTO" << endl << "[2]FINALIZAR COMPRA" << endl;
                    cin >>opcaoCompra;
                    if (verificaEstoque(codigoProd, quantidadeProd) == 1){// verifica se tem a quantidade de produto no estoque
                        emFalta ++;
                    }
                    if (opcaoCompra == 1){
                        continue;
                    }else{
                        if (emFalta != 0){
                            cout << "ERRO: SEM ESTOQUE."<< endl;
                            getchar();
                            getchar();
                            system("clear");
                            break;                        
                        }
                        atualizaProduto(carrinho);
                        printcarrinho(carrinho);
                        valor = fechaConta(carrinho);
                        if (eSocio == 1){
                            cout << "VALOR: "<< valor << endl;
                            cout << "DESCONTO: 15%"<< endl;
                            valorFinal = valor - (valor*0.15);
                            cout <<"VALOR FINAL:" << valorFinal<< endl; 
                            printf("%.2f", valorFinal);
                        }else{
                             cout << "VALOR: "<< valor << endl;
                        }
                        printf("\t\tOBRIGADO PELA PREFERENCIA\n");
                        addRecomendacao(cpf, carrinho);
                        carrinho.clear();
                        getchar();
                        getchar();
                        system("clear");
                        break;
                    }
                }while (opcaoCompra !=0 && opcaoCompra < 2);
                emFalta = 0;
            }else {
                cout << "CADASTRAR NOVO CLIENTE? [S/N]"<< endl;
                cin >> cliente_cadastrado;
                if (cliente_cadastrado=='s' || cliente_cadastrado == 'S'){
                    cout << "O CLIENTE DESEJA SER SOCIO? [S/N]"<< endl;
                    cin >> cliente_cadastrado;
                    getchar();
                    system("clear");
                    if (cliente_cadastrado=='s' || cliente_cadastrado == 'S'){// adicionando novo cliente socio
                        socio = true;
                        socio_novo.setSocio(socio);
                        cout << "NOME DO CLIENTE: ";
                        cin >> nome_novo;
                        socio_novo.setNome(nome_novo);
                        cout << "IDADE: ";
                        cin >> idade_novo;
                        socio_novo.setIdade(idade_novo);
                        cout<< "RG: ";
                        cin >> rg_novo;
                        socio_novo.setRG(rg_novo);
                        cout << "CPF: ";
                        cin >> cpf_novo;
                        socio_novo.setCPF(cpf_novo);
                        cout << "EMAIL: ";
                        cin >> email_novo;
                        socio_novo.setEmail(email_novo);
                        arqCliente.open ("doc/Clientes/Socio/"+ cpf_novo + ".txt", ios::out);
                        if (arqCliente.is_open()){
                            arqCliente << socio_novo.getNome()<< endl;
                            arqCliente << socio_novo.getIdade()<< endl;
                            arqCliente << socio_novo.getRG()<< endl;
                            arqCliente << socio_novo.getCPF()<< endl;
                            arqCliente<< socio_novo.getEmail()<< endl;
                        }else{
                            cout<<"ERRO: NAO FOI POSSIVEL CADASTRAR CLIENTE"<< endl << "TENTE NOVAMENTE"<< endl;
                            system("clear");
                            break;
                        }
                        printf ("\tNOVO CLIENTE SOCIO CADASTRADO!!!\n");
                        getchar();
                        getchar();
                        system("clear");
                        arqCliente.close();
                        break;
                    }else {//adicionando novo cliente
                        socio = false;
                        cliente_novo.setSocio(socio);
                        cout << "NOME DO CLIENTE: ";
                        cin >> nome_novo;
                        cliente_novo.setNome(nome_novo);
                        cout << "IDADE: ";
                        cin >> idade_novo;
                        cliente_novo.setIdade(idade_novo);
                        cout<< "RG: ";
                        cin >> rg_novo;
                        cliente_novo.setRG(rg_novo);
                        cout << "CPF: ";
                        cin >> cpf_novo;
                        cliente_novo.setCPF(cpf_novo);
                        cout << "EMAIL: ";
                        cin >> email_novo;
                        cliente_novo.setEmail(email_novo);
                        arqCliente.open ("doc/Clientes/Cliente/"+ cpf_novo + ".txt", ios::out);
                        if (arqCliente.is_open()){
                            arqCliente << cliente_novo.getNome()<< endl;
                            arqCliente << cliente_novo.getIdade()<< endl;
                            arqCliente << cliente_novo.getRG()<< endl;
                            arqCliente << cliente_novo.getCPF()<< endl;
                            arqCliente<< cliente_novo.getEmail()<< endl;
                        }else{
                            cout<<"ERRO: NAO FOI POSSIVEL CADASTRAR CLIENTE"<< endl << "TENTE NOVAMENTE"<< endl;
                            getchar();
                            getchar();
                            system("clear");
                            break;
                        }
                        printf ("\tNOVO CLIENTE CADASTRADO!!!\n");
                        arqCliente.close();
                        getchar();
                        getchar();
                        system("clear");
                        break;
                    }
                }else{
                    break;
                }
            }          
            cout << endl << endl;
            break;
         case 2 :
            printf("\t\tESTOQUE\n");
            cout << "[1]ATUALIZAR QUANTIDADE DE PRODUTO"<< endl<< "[2]ADICIONAR NOVO PRODUTO" << endl << "[0]VOLTAR"<< endl;
            cin >> opcaoProduto;
            getchar();
            system("clear");
            if (opcaoProduto == 1){
                cout << endl << "CODIGO DO PRODUTO: ";
                cin >> codigoProd;
                if (buscaProduto(codigoProd) != 0 ){
                    cout << "PRODUTO INEXISTENTE.";
                    getchar();
                    getchar();
                    system("clear");
                }else{
                    cout << "NOVA QUANTIDADE: ";
                    cin >> quantidadeProd;
                    if (atualizaQnt(quantidadeProd, codigoProd) == 0){
                        printProduto(codigoProd);
                        getchar();
                        getchar();
                        system ("clear");
                        break;
                    }else {
                        cout << "NAO FOI POSSIVEL ATUALIZAR O PRODUTO." << endl;
                        getchar();
                        getchar();
                        system ("clear");
                        break;
                    }
                }
            }else if (opcaoProduto == 2){
                printf("\t\tPRODUTO NOVO\n");
                cout << "CODIGO: ";
                cin >> codigoProd;
                produto_novo.setCodProd(codigoProd);
                cout << "CATEGORIA: ";
                cin >> categoriaProd;
                produto_novo.setCategoria(categoriaProd);
                cout << "MARCA: ";
                cin >> marcaProd;
                produto_novo.setMarcaProd(marcaProd);
                cout << "NOME: ";
                cin >> nomePod;
                produto_novo.setNomeProd(nomePod);
                cout << "QUANTIDADE: ";
                cin >> quantidadeProd;
                produto_novo.setQntProd(quantidadeProd);
                cout << "PRECO: $";
                cin >> precoProd;
                produto_novo.setPrecoProd(precoProd);
                arqProduto.open ("doc/Produtos/"+ codigoProd + ".txt", ios::out);
                if (arqProduto.is_open()){
                    arqProduto << produto_novo.getCodProd() << endl;
                    arqProduto << produto_novo.getCategoria()<<endl;
                    arqProduto << produto_novo.getMarcaProd()<< endl;
                    arqProduto << produto_novo.getNomeProd()<< endl;
                    arqProduto<< produto_novo.getQntProd()<< endl;
                    arqProduto << produto_novo.getPrecoProd()<< endl;  
                }else{
                    cout<<"ERRO: NAO FOI POSSIVEL CADASTRAR PRODUTO"<< endl << "TENTE NOVAMENTE"<< endl;
                    getchar();
                    getchar();
                    system("clear");
                    break;
                } 
                printf ("\tNOVO PRODUTO CADASTRADO!!!\n");
                arqProduto.close();
                getchar();
                getchar();
                system("clear");
                break;
            }else{
                break;
            }    
            break;
         case 3 :
            printf("\t\tLOGIN\n");
            cout<<"CPF DO SOCIO: ";
            cin >> cpf;
            if (seraqueeSocio (cpf) == 1){
                Recomenda(cpf);
                getchar();
                getchar();
                system("clear");
                break;
            }else {
                cout << "CLIENTE NAO E SOCIO" << endl;
                getchar();
                getchar();
                system("clear");
                break;
            } 
            break;
        default:
            cout << "Encerrando ..............."<< endl;
            exit (-1);
        }
    }while (opcao > 0 && opcao< 4);


    return 0;
}

int login (string cpf){
    ifstream listaSocio;
    listaSocio.open ("doc/Clientes/Socio/"+ cpf + ".txt");
    ifstream listaCliente;
    listaCliente.open ("doc/Clientes/Cliente/"+ cpf + ".txt");
    if (listaSocio.is_open()){
        listaSocio.close();
        listaCliente.close();
        return 0;
    } else if (listaCliente.is_open()){
        listaSocio.close();
        listaCliente.close();
        return 0;
    }else {
        return -1;
    }
}

int seraqueeSocio (string cpf){
    ifstream listaSocio;
    listaSocio.open ("doc/Clientes/Socio/"+ cpf + ".txt");
    if (listaSocio.is_open()){
        listaSocio.close();
        return 1;
    }else {
        return 0;
    }
}

int buscaProduto ( string cod){
    ifstream listaProduto;
    listaProduto.open ("doc/Produtos/"+ cod + ".txt", ios::in);
    if (listaProduto.is_open()){
        listaProduto.close();
        return 0;
    }else {
        return -1;
    }
}

int verificaEstoque(string cod, int qnt){
    int quantidade;string texto;
    ifstream listaProduto;
    listaProduto.open ("doc/Produtos/"+ cod + ".txt", ios::in);
    if (listaProduto.is_open()){
        for (int i = 1; getline(listaProduto,texto) && i <= 5; i++){
            if (i ==5){
                quantidade = stoi(texto);
                if (quantidade > qnt)
                {
                    listaProduto.close();
                    return 1;
                }
            }
        }
        listaProduto.close();
        return 0;
    }else {
        return -1;
    }
}

void atualizaProduto(vector <Carrinho *> &car){
    Produto atualiza;
    int qnt;
    string cod,texto;
    ifstream listaProduto;
    ofstream arqAtualizado;
    for (int i = 0; i < (car.size()); i++){
        qnt = car[i]->getQntProd();
        cod = car[i]->getCodProd();
        listaProduto.open ("doc/Produtos/"+ cod + ".txt", ios::in);
        if (listaProduto.is_open()){
            for (int j = 1; getline(listaProduto,texto) && j <= 6; j++){
                if (j ==1){
                    atualiza.setCodProd(texto);}
                if (j ==2){
                    atualiza.setCategoria(texto);}
                if (j ==3){
                    atualiza.setMarcaProd(texto);}
                if (j ==4){
                    atualiza.setNomeProd(texto);}
                if (j ==5){
                    atualiza.setQntProd(stoi(texto));}
                if (j ==6){
                    atualiza.setPrecoProd(stof(texto));}
            }
        }
        listaProduto.close();
        qnt = (atualiza.getQntProd()) - qnt;
        atualiza.setQntProd(qnt);
        arqAtualizado.open ("doc/Produtos/"+ cod + ".txt", ios::out);
        if (arqAtualizado.is_open()){
            arqAtualizado << atualiza.getCodProd() << endl;
            arqAtualizado << atualiza.getCategoria() << endl;
            arqAtualizado << atualiza.getMarcaProd() << endl;
            arqAtualizado << atualiza.getNomeProd() << endl;
            arqAtualizado<< atualiza.getQntProd() << endl;
            arqAtualizado << atualiza.getPrecoProd() << endl;        }
        arqAtualizado.close();
    }
}

void printcarrinho(vector <Carrinho *> &car){
    Produto listaProduto;
    string cod,texto;
    int qnt;
    ifstream produtoLista;
    printf("\t\tLISTA DE PRODUTOS NO CARRINHO\n");
    for (int i = 0; i < (car.size()); i++){
        qnt = car[i]->getQntProd();
        cod = car[i]->getCodProd();
        produtoLista.open ("doc/Produtos/"+ cod + ".txt", ios::in);
        if (produtoLista.is_open()){
            for (int j = 1; getline(produtoLista,texto) && j <= 6; j++){
                if (j ==1){
                    listaProduto.setCodProd(texto);}
                if (j ==2){
                    listaProduto.setCategoria(texto);}
                if (j ==3){
                    listaProduto.setMarcaProd(texto);}
                if (j ==4){
                    listaProduto.setNomeProd(texto);}
                if (j ==5){
                    listaProduto.setQntProd(qnt);}
                if (j ==6){
                    listaProduto.setPrecoProd(stof(texto));}
            }
            cout << listaProduto.getCodProd() <<"    "<< listaProduto.getCategoria()<< "    " << listaProduto.getMarcaProd() << endl;
            cout << listaProduto.getQntProd()<< "    "<< listaProduto.getNomeProd() << "    " << listaProduto.getPrecoProd() << endl << endl;           
        }
        produtoLista.close();
        
    }
}

float fechaConta(vector <Carrinho *> &car){
    Produto listaProduto;
    string cod,texto;
    int qnt;
    float valor= 0.0;
    ifstream produtoLista;
    for (int i = 0; i < (car.size()); i++){
        qnt = car[i]->getQntProd();
        cod = car[i]->getCodProd();
        produtoLista.open ("doc/Produtos/"+ cod + ".txt", ios::in);
        if (produtoLista.is_open()){
            for (int j = 1; getline(produtoLista,texto) && j <= 6; j++){
                if (j ==6){
                    listaProduto.setPrecoProd(stof(texto));}
            }
            valor = valor + (listaProduto.getPrecoProd()* qnt);      
        }
        produtoLista.close();
    }
    return valor;

}

int atualizaQnt(int qnt, string cod){
    Produto atualiza;
    string texto;
    ifstream listaProduto;
    ofstream arqAtualizado;
    if (qnt <= 0){
        return -1;
    }
    listaProduto.open ("doc/Produtos/"+ cod + ".txt", ios::in);
    if (listaProduto.is_open()){
        for (int j = 1; getline(listaProduto,texto) && j <= 6; j++){
            if (j ==1){
                atualiza.setCodProd(texto);}
            if (j ==2){
                atualiza.setCategoria(texto);}
            if (j ==3){
                atualiza.setMarcaProd(texto);}
            if (j ==4){
                atualiza.setNomeProd(texto);}
            if (j ==5){
                atualiza.setQntProd(qnt);}
            if (j ==6){
                atualiza.setPrecoProd(stof(texto));}
        }
    }
        listaProduto.close();
        arqAtualizado.open ("doc/Produtos/"+ cod + ".txt", ios::out);
        if (arqAtualizado.is_open()){
            arqAtualizado << atualiza.getCodProd() << endl;
            arqAtualizado << atualiza.getCategoria() << endl;
            arqAtualizado << atualiza.getMarcaProd() << endl;
            arqAtualizado << atualiza.getNomeProd() << endl;
            arqAtualizado<< atualiza.getQntProd() << endl;
            arqAtualizado << atualiza.getPrecoProd() << endl;        }
        arqAtualizado.close();
        return 0;
    
}

void printProduto(string cod){
    Produto listaProduto;
    string texto;
    ifstream produtoLista;
    printf("\t\tPRODUTO ATUALIZADO\n");
    produtoLista.open ("doc/Produtos/"+ cod + ".txt", ios::in);
    if (produtoLista.is_open()){
        for (int j = 1; getline(produtoLista,texto) && j <= 6; j++){
            if (j ==1){
                listaProduto.setCodProd(texto);}
            if (j ==2){
                listaProduto.setCategoria(texto);}
            if (j ==3){
                listaProduto.setMarcaProd(texto);}
            if (j ==4){
                listaProduto.setNomeProd(texto);}
            if (j ==5){
                listaProduto.setQntProd(stoi(texto));}
            if (j ==6){
                listaProduto.setPrecoProd(stof(texto));}
        }
            cout << listaProduto.getCodProd() <<"    "<< listaProduto.getCategoria()<< "    " << listaProduto.getMarcaProd() << endl;
            cout << listaProduto.getQntProd()<< "    "<< listaProduto.getNomeProd() << "    " << listaProduto.getPrecoProd() << endl << endl;           
        }
        produtoLista.close();
}

void addRecomendacao (string cpf, vector <Carrinho *> &car){
    vector <Carrinho *> listaRecomenda;
    string texto, cod;
    fstream arqRecomenda;
    fstream arqAux;

    int count = 0, pos = 0;
    arqRecomenda.open ("doc/Recomendacoes/"+ cpf + ".txt", ios::in|ios::out);
    if (arqRecomenda.is_open()){
        for (int i = 1; getline(arqRecomenda,texto) || texto != " "; i++){
            count++;
            if ((i%2) == 0){
                listaRecomenda[pos]->setQntProd(stoi(texto));
            }else {
                listaRecomenda[pos]->setCategoria(texto);
            }
            if (count == 2){
                pos++;
                count = 0;
            }   
        }
    }
    arqRecomenda.close();
    int aux = 0, achou = 0;
    for (int k = 0; k < (car.size()); k++){
        cod = car[k]->getCodProd();
        arqRecomenda.open ("doc/Produtos/"+ cod + ".txt", ios::in);
        if (arqRecomenda.is_open()){
            for (int j = 1; getline(arqRecomenda,texto) && j <= 6; j++){
                if (j ==2){
                    for(int l = 0; l < (listaRecomenda.size()); l++){
                        if (listaRecomenda[l]->getCategoria() == texto){
                           aux = listaRecomenda[l]->getQntProd();
                           aux++;
                           listaRecomenda[l]->setQntProd(aux);
                           achou++;
                           break;
                        }
                    }
                    if (achou == 0){
                        listaRecomenda.push_back(new Carrinho (1, texto));
                    }else {
                        achou = 0;
                    }
                }        

            }
        }
    }
        arqRecomenda.close();
        arqRecomenda.open("doc/Recomendacoes/"+ cpf + ".txt", ios::out);
        for (int i = 0; i< listaRecomenda.size(); i ++){
            arqRecomenda << listaRecomenda[i]->getCategoria()<< endl;
            arqRecomenda << listaRecomenda[i]->getQntProd()<< endl;
        }
        arqRecomenda.close();
        string cod2;
        
        for (int k = 0; k < (car.size()); k++){
        cod = car[k]->getCodProd();
        arqRecomenda.open ("doc/Produtos/"+ cod + ".txt", ios::in);
        if (arqRecomenda.is_open()){
            for (int j = 1; getline(arqRecomenda,texto) && j <= 6; j++){
                
                if (j==1){
                cod2 = texto;
                }
                if (j ==2){
                    arqAux.open("doc/Recomendacoes/"+cpf+"-"+texto+".txt",ios::ate);
                    if (arqAux.is_open()){
                        arqAux << cod2 << endl;
                    }
                    arqAux.close();  
                } 
            }

        }        
    }
}

bool compareQnt(Carrinho left, Carrinho  right)
{
    return left.getQntProd() > right.getQntProd();
}

void Recomenda (string cpf){
    vector <Carrinho *> listaRecomenda;
    vector <string> elemRecomenda(10);
    string texto, texto2;
    fstream arqRecomenda;
    fstream arqAux;

    int count = 0, pos = 0;
    arqRecomenda.open ("doc/Recomendacoes/"+ cpf + ".txt", ios::in|ios::out);
    if (arqRecomenda.is_open()){
        for (int i = 1; getline(arqRecomenda,texto) || texto != " "; i++){
            count++;
            if ((i%2) == 0){
                listaRecomenda[pos]->setQntProd(stoi(texto));
            }else {
                listaRecomenda[pos]->setCategoria(texto);
            }
            if (count == 2){
                pos++;
                count = 0;
            }   
        }
    }
    arqRecomenda.close();
    sort(listaRecomenda.begin(), listaRecomenda.end());
    int qnt;
    pos = 0;
    for(int i = 0; i < (listaRecomenda.size()); i++){
        qnt = listaRecomenda[i]->getQntProd();
        texto2 = listaRecomenda[i]->getCategoria();
        for (int j= 0;j<qnt;j++){
            elemRecomenda.push_back(texto2);
            pos++;
            if (pos == 10){break;}
        }
        if (pos == 10){break;}
    } 
    string aux = " ", aux2;
    int novaPos= 0; 
    for (int i = 0 ; i < 10; i++){
        aux2 = elemRecomenda[novaPos];
        arqRecomenda.open("doc/Recomendacoes/" + cpf + "-" + aux2 + ".txt",ios::in);
        if (arqRecomenda.is_open()){
        for (int j = 1; getline(arqRecomenda,texto) || novaPos <= 10; j++){
            if (aux == texto){
                continue;
            }else {
                aux = texto;
                arqAux.open ("doc/Produtos/"+ aux + ".txt", ios::in);
                if (arqAux.is_open()){
                    for (int k = 1; getline(arqAux,texto) && k <= 6; k++){
                        if (k ==1){
                            cout << texto << "    ";}
                        if (k ==2){
                            cout << texto<< endl;}
                        if (k ==3){
                            cout << texto << "    ";}
                        if (k ==4){
                            cout << texto << endl;}
                    }
                        novaPos ++;
                }
            }
                arqAux.close();
        }
            arqRecomenda.close();
        } 
        if (novaPos == 10){break;}       
    }
}