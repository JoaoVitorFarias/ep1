# EP1 - OO

* DISCIPLINA: Orientação a Objetos
* TURMA : B
* PROFESSORA: Carla Silva Rocha Aguiar
* ALUNO: João Vitor Lopes de Farias
* MATRÍCULA: 18/0020251





## BIBLIOTECAS UTILIZADAS

*  *iostream*; 

*  *vector*;

*  *strings*;

*  *fstream*;

*  *algorithm*;



## DESENVOLVIMENTO

O programa foi desenvolvido  no Linux Ubuntu 18.04 LTS, usando o Visual Studio Code como IDE.


## INSTRUÇÕES DE USO

O programa está sendo compilado usando o comando ‘make’. Para iniciar o programa basta digitar o comando ‘make run’. Após o programa inicializado será exibido na tela um menu com 4 opções: 1ª modo venda, 2ª modo estoque, 3ª modo recomendação e 4ª sair (encerra o programa).

### Modo venda:

Primeiramente pergunta se o cliente já é cadastrado. Caso não seja, é perguntado se deseja cadastrar, após a validação ele pergunta se o novo cliente irá ser sócio ou não, em seguida realiza o cadastro, depois volta para o menu principal.

Caso o cliente seja cadastrado, ele informa o CPF para fazer uma verificação e depois é exibido a área de compra. É pedido para informar o código do produto (verifica a existência do produto) e depois a quantidade , esse processo ocorre até o cliente finalizar a compra. Posteriormente verifica se há os produtos em estoque, se não houver,  a compra não é finalizada e volta ao menu principal. Caso tenha os produtos em estoque a compra é finalizada, o valor final é exibido na tela (se o cliente for sócio ele ganha 15% de desconto no valor da compra) e finaliza o modo venda voltando para o menu principal.

Exemplo de cadastro de Cliente:
* Nome do Cliente: Joao
* Idade: 19
* RG:123456
* CPF:12345678901
* Email:joao@gmail.com


### Modo estoque:

No modo estoque é exibido duas opções, para atualizar a quantidade de um produto ou para adicionar um novo produto. Para atualizar o quantidade é necessário informar o código do produto (ocorre uma verificação para saber da existência do produto) e a nova quantidade. Após  o valor ser atualizado o produto é exibido na tela e volta para o menu principal.

Na segunda opção, para adicionar um novo produto basta inserir as informações necessárias e o cadastro já estará realizado, voltando para o menu principal em seguida.

Exemplo de cadastro de produto:
* Código: 123456
* Categoria: Alimentacao
* Marca: Paozinho
* Nome: Pao
* Quantidade: 56
* Preço: 2.30


### Modo recomendação:

Inicialmente é pedido o CPF de um cliente sócio, realiza uma verificação e em seguida exibe na tela até 10 produtos recomendados, depois volta para o menu principal.


