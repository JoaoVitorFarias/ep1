#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include <string>

using namespace std;

class Produto{
    private:
        string codProd;
        string categoria;
        string nomeProd;
        string marcaProd;
        int qntProd;
        float precoProd;
        
    public:
        Produto();
        Produto(string categoria, string nomeProd, string marcaProd, float precoProd, int qntProd, string codProd);
        ~Produto();
        virtual void setCategoria(string categoria);
        virtual string getCategoria();
        void setNomeProd(string nomeProd);
        string getNomeProd();
        void setMarcaProd(string marcaProd);
        string getMarcaProd();
        void setPrecoProd(float precoProd);
        float getPrecoProd();
        virtual void setQntProd (int qntProd);
        virtual int getQntProd();
        virtual void setCodProd(string codProd);
        virtual string getCodProd();
};

#endif