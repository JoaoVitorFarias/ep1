#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <string>

using namespace std;

class Cliente {
    private:
        bool socio;
        string nome;
        int idade;
        string rg;
        string cpf;
        string email;
    public:
        Cliente ();
        Cliente (bool socio);
        Cliente (bool socio, string nome, int idade, string rg, string cpf, string email);
        ~Cliente ();
        void setSocio(bool socio);
        bool getSocio();
        void setNome(string nome);
        string getNome ();
        void setIdade (int idade);
        int getIdade();
        void setRG(string rg);
        string getRG();
        void setCPF(string cpf);
        string getCPF ();
        void setEmail(string email);
        string getEmail();
};

#endif