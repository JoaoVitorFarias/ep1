#ifndef CARRINHO_HPP
#define CARRINHO_HPP

#include "produto.hpp"
#include <string>

using namespace std;

class Carrinho : public Produto {
    public:
        Carrinho ();
        Carrinho (int qntProd, string codProd);
        ~Carrinho ();       
};

#endif